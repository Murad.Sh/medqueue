package com.example.medqueue.infrastructure.repository;

import com.example.medqueue.domain.entity.UserEntity;
import com.example.medqueue.domain.repository.UserRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserJpaRepository extends JpaRepository<UserEntity, Long>, UserRepository {

}
