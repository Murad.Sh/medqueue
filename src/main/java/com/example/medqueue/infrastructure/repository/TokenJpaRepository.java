package com.example.medqueue.infrastructure.repository;

import com.example.medqueue.domain.entity.TokenEntity;
import com.example.medqueue.domain.repository.TokenRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenJpaRepository extends JpaRepository<TokenEntity, Long>, TokenRepository {

}
