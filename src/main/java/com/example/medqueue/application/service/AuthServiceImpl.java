package com.example.medqueue.application.service;

import com.example.medqueue.application.dto.TokenDTO;
import com.example.medqueue.application.exception.EmailAlreadyExistsException;
import com.example.medqueue.application.exception.NotFoundException;
import com.example.medqueue.application.util.enums.RoleType;
import com.example.medqueue.application.util.enums.TokenType;
import com.example.medqueue.domain.entity.TokenEntity;
import com.example.medqueue.domain.entity.UserEntity;
import com.example.medqueue.domain.repository.TokenRepository;
import com.example.medqueue.domain.repository.UserRepository;
import com.example.medqueue.domain.service.AuthService;
import com.example.medqueue.presentation.config.JwtService;
import com.example.medqueue.presentation.presenter.auth.SignInRequest;
import com.example.medqueue.presentation.presenter.auth.SignUpRequest;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuthServiceImpl implements AuthService {
    final UserRepository userRepository;
    final AuthenticationManager authenticationManager;
    final TokenRepository tokenRepository;
    final JwtService jwtService;
    final PasswordEncoder passwordEncoder;

    @SneakyThrows
    @Override
    public TokenDTO signIn(SignInRequest signInRequest) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        signInRequest.getEmail(),
                        signInRequest.getPassword()
                )
        );
        var user = userRepository.findByEmail(signInRequest.getEmail()).orElseThrow(NotFoundException::new);
        var accessToken = jwtService.generateToken(user);
        saveUserToken(user, accessToken);

        return TokenDTO.builder().accessToken(accessToken).build();
    }

    @SneakyThrows
    @Override
    public TokenDTO signUp(SignUpRequest signUpRequest) {
        var user = UserEntity.builder()
                .name(signUpRequest.getName())
                .surname(signUpRequest.getSurname())
                .email(signUpRequest.getEmail())
                .password(passwordEncoder.encode(signUpRequest.getPassword()))
                .role(RoleType.USER)
                .build();
        if (userRepository.findByEmail(user.getEmail()).isPresent())
            throw new EmailAlreadyExistsException();
        var savedUser = userRepository.save(user);
        var accessToken = jwtService.generateToken(savedUser);

        saveUserToken(savedUser, accessToken);
        return TokenDTO.builder().accessToken(accessToken).build();
    }

    private void saveUserToken(UserEntity user, String token) {
        var tokenEntity = TokenEntity
                .builder()
                .user(user)
                .token(token)
                .tokenType(TokenType.BEARER)
                .expired(false)
                .revoked(false).build();
        tokenRepository.save(tokenEntity);
    }
}
