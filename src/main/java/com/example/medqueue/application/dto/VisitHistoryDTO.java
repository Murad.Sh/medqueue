package com.example.medqueue.application.dto;

import com.example.medqueue.application.util.enums.VisitPurpose;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class VisitHistoryDTO {
    Long id;
    UserDTO userDTO;
    PatientDTO patientDTOId;
    ClinicDTO clinicDTOId;
    DoctorDTO doctorDTOId;
    LocalDate visitDate;
    VisitPurpose visitPurpose;
    String note;
}
