package com.example.medqueue.application.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ReviewDTO {
    Long id;
    UserDTO userDTOId;
    DoctorDTO doctorDTOId;
    ClinicDTO clinicDTOId;
    Integer rating;
    String comment;
}
