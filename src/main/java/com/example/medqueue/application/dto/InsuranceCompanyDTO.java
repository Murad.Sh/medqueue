package com.example.medqueue.application.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class InsuranceCompanyDTO {
    Long id;
    String insuranceCompanyName;
    PhoneDTO phoneDTOId;
    EmailDTO emailDTOId;
}
