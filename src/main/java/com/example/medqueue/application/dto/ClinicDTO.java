package com.example.medqueue.application.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ClinicDTO {
    Long id;
    String clinicName;
    ClinicBranchDTO clinicBranchDTOId;
    EmailDTO emailDTOId;
    DoctorDTO doctorDTOId;
}
