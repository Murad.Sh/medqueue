package com.example.medqueue.application.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.time.LocalTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuditLogDTO {
    Long id;
    UserDTO userDTOId;
    String action;
    LocalDate dateStamp;
    LocalTime timeStamp;
    String description;
}
