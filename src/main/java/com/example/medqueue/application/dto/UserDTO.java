package com.example.medqueue.application.dto;

import com.example.medqueue.application.util.enums.RoleType;
import lombok.*;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDTO {
    Long id;
    String userName;
    String password;
    EmailDTO emailDTOId;
    PhoneDTO phoneDTOId;
    RoleType userType;
}
