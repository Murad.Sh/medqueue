package com.example.medqueue.application.dto;

import com.example.medqueue.application.util.enums.AppointmentStatus;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.time.LocalTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AppointmentDTO {
    Long id;
    String bookingUserId; //TODO: fix that field
    PatientDTO patientDTOId;
    DoctorDTO doctorDTOId;
    LocalDate appointmentDate;
    LocalTime appointmentTime;
    AppointmentStatus appointmentStatus;
}
