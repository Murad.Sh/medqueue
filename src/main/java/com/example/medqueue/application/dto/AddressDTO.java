package com.example.medqueue.application.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AddressDTO {
    Long id;
    String flatDetails;
    String houseDetails;
    String streetDetails;
    String districtDetails;
    String cityDetails;
    String countryDetails;
}
