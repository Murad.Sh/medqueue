package com.example.medqueue.application.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PatientDTO {
    Long id;
    UserDTO userDTOId;
    String firstName;
    String lastName;
    String passportSeries;
    String passportNumber;
    String passportFin;
    PhoneDTO phoneDTOId;
    EmailDTO emailDTOId;
}
