package com.example.medqueue.application.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class NotificationDTO {
    Long id;
    UserDTO userDTOId;
    String content;
    LocalDate dateSent;
    Boolean isRead;
}
