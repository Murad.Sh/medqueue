package com.example.medqueue.application.dto;

import com.example.medqueue.application.util.enums.WeekDay;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ScheduleDTO {
    Long id;
    DoctorDTO doctorDTOId;
    WeekDay weekDay;
    LocalTime startTime;
    LocalTime endTime;
}
