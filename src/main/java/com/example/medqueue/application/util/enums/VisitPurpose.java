package com.example.medqueue.application.util.enums;

public enum VisitPurpose {
    GENERAL_CONSULTATION,
    FOLLOW_UP,
    VACCINATION,
    DIAGNOSTIC_TEST,
    EMERGENCY_CARE,
    PRESCRIPTION_REFILL,
    SPECIALIST_CONSULTATION,
    THERAPY_SESSION,
    SURGICAL_CONSULTATION,
    ANNUAL_CHECKUP,
    OTHER
}
