package com.example.medqueue.application.util.enums;

public enum AppointmentStatus {
    CANCELED,
    DONE,
    PLANNED
}
