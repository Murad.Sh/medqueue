package com.example.medqueue.application.util.enums;

public enum RoleType {
    USER,
    DOCTOR,
    CLINIC,
    INSURANCE_COMPANY,
    ADMIN
}
