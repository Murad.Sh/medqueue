package com.example.medqueue.presentation.controller;

import com.example.medqueue.domain.service.AuthService;
import com.example.medqueue.presentation.presenter.auth.AuthenticationResponse;
import com.example.medqueue.presentation.presenter.auth.SignInRequest;
import com.example.medqueue.presentation.presenter.auth.SignUpRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthController {
    @Autowired
    private AuthService authService;

    @RequestMapping(path = "/sign-in", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> signIn(@Valid @RequestBody SignInRequest request) {
        var token = authService.signIn(request);
        return ResponseEntity.ok().body(AuthenticationResponse.builder().accessToken(token.getAccessToken()).build());
    }

    @RequestMapping(path = "/sign-up", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> signUp(@Valid @RequestBody SignUpRequest request) {
        var token = authService.signUp(request);
        return ResponseEntity.accepted().body(AuthenticationResponse.builder().accessToken(token.getAccessToken()).build());
    }
}
