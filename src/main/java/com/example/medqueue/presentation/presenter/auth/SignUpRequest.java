package com.example.medqueue.presentation.presenter.auth;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class SignUpRequest {
    @NotEmpty(message = "Name is required.")
    @Size(min = 3, max = 50, message = "Name must be between 3 and 50 characters.")
    private String name;

    @NotEmpty(message = "Surname is required.")
    @Size(min = 3, max = 50, message = "Surname must be between 3 and 50 characters.")
    private String surname;

    @NotEmpty(message = "Password is required.")
    @Size(min = 6, max = 40, message = "Password must be between 6 and 40 characters.")
    private String password;

    @NotEmpty(message = "Email is required.")
    @Email(message = "Enter a valid email address.")
    private String email;
}
