package com.example.medqueue.domain.service;

import com.example.medqueue.application.dto.TokenDTO;
import com.example.medqueue.presentation.presenter.auth.SignInRequest;
import com.example.medqueue.presentation.presenter.auth.SignUpRequest;

public interface AuthService {
    TokenDTO signIn(SignInRequest signInRequest);

    TokenDTO signUp(SignUpRequest signUpRequest);
}
