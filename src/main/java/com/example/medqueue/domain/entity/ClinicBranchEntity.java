package com.example.medqueue.domain.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "clinic_branches")
public class ClinicBranchEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column
    String branchName;

    @OneToMany(mappedBy = "clinic_branches", cascade = CascadeType.ALL)
    List<PhoneEntity> phoneEntityList;

    @OneToMany(mappedBy = "clinic_branches", cascade = CascadeType.ALL)
    List<AddressEntity> addressEntityList;
}
