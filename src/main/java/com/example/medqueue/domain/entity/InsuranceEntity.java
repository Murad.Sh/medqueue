package com.example.medqueue.domain.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.experimental.FieldDefaults;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "insurance_companies")
public class InsuranceEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column
    @NotBlank
    String email;

    @OneToMany(mappedBy = "insurance_companies", cascade = CascadeType.ALL)
    List<PhoneEntity> phoneEntityList;

    @ManyToMany
    @JoinTable(
            name = "partnerships",
            joinColumns = @JoinColumn(name = "insurance_companies_id"),
            inverseJoinColumns = @JoinColumn(name = "clinics_id")
    )
    List<ClinicEntity> clinicEntityList;
}
