package com.example.medqueue.domain.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.experimental.FieldDefaults;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "patients")
public class PatientEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "users_id")
    UserEntity userEntityId;

    @Column
    @NotBlank
    String firstName;

    @Column
    @NotBlank
    String lastName;

    @Column
    @NotBlank
    String passportSeries;

    @Column
    @NotBlank
    String passportNumber;

    @Column
    @NotBlank
    String passportFin;

    @OneToMany(mappedBy = "patients", cascade = CascadeType.ALL)
    List<PhoneEntity> phoneEntityId;

    @OneToMany(mappedBy = "patients", cascade = CascadeType.ALL)
    List<EmailEntity> emailEntityId;
}
