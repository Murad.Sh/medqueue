package com.example.medqueue.domain.entity;

import com.example.medqueue.application.util.enums.VisitPurpose;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "visit_histories")
public class VisitHistoryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "users_id")
    UserEntity userEntity;

    @OneToOne(mappedBy = "visit_histories")
    PatientEntity patientEntity;

    @OneToOne(mappedBy = "visit_histories")
    ClinicEntity clinicEntity;

    @OneToOne(mappedBy = "visit_histories")
    DoctorEntity doctorEntity;

    @Column
    @NotBlank
    LocalDate visitDate;

    @Column
    @NotBlank
    VisitPurpose visitPurpose;

    @Column
    String note;
}
