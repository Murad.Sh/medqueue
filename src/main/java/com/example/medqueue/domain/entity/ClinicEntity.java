package com.example.medqueue.domain.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.experimental.FieldDefaults;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "clinics")
public class ClinicEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column
    @NotBlank
    String clinicName;

    @OneToMany(mappedBy = "clinics", cascade = CascadeType.ALL)
    List<ClinicBranchEntity> clinicBranchEntityList;

    @OneToMany(mappedBy = "clinics", cascade = CascadeType.ALL)
    List<EmailEntity> emailEntityList;

    @OneToMany(mappedBy = "clinics", cascade = CascadeType.ALL)
    List<DoctorEntity> doctorEntityList;

    @ManyToMany
    @JoinTable(
            name = "clinics_departments",
            joinColumns = @JoinColumn(name = "clinics_id"),
            inverseJoinColumns = @JoinColumn(name = "departments_id")
    )
    List<DepartmentEntity> departmentEntityList;
}
