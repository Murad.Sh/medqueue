package com.example.medqueue.domain.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "reviews")
public class ReviewEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "users_id")
    UserEntity userEntity;

    @OneToOne(mappedBy = "reviews")
    DoctorEntity doctorEntity;

    @OneToOne(mappedBy = "reviews")
    ClinicEntity clinicEntity;

    @Column
    @NotBlank
    Integer rating;

    @Column
    @NotBlank
    String comment;
}
