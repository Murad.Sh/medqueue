package com.example.medqueue.domain.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "addresses")
public class AddressEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column
    String flatDetails;

    @Column
    String houseDetails;

    @Column
    String streetDetails;

    @Column
    String districtDetails;

    @Column
    String cityDetails;

    @Column
    String countryDetails;
}
