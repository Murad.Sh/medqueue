package com.example.medqueue.domain.entity;

import com.example.medqueue.application.util.enums.WeekDay;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.experimental.FieldDefaults;
import java.time.LocalTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "schedules")
public class ScheduleEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column
    @NotBlank
    WeekDay weekDay;

    @Column
    @NotBlank
    LocalTime startTime;

    @Column
    @NotBlank
    LocalTime endTime;

    @OneToMany
    List<DoctorEntity> doctorEntityList;
}
