package com.example.medqueue.domain.entity;

import com.example.medqueue.application.util.enums.AppointmentStatus;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "appointments")
public class AppointmentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column
    String bookingUserId; //TODO: fix that field

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "patients_id")
    PatientEntity patientEntityId;

    @OneToMany(mappedBy = "appointments", cascade = CascadeType.ALL)
    List<DoctorEntity> doctorEntityId;

    @Column
    LocalDate appointmentDate;

    @Column
    LocalTime appointmentTime;

    @Column
    AppointmentStatus appointmentStatus;
}
