package com.example.medqueue.domain.repository;

import com.example.medqueue.domain.entity.TokenEntity;

import java.util.Optional;

public interface TokenRepository {
    Optional<TokenEntity> findByToken(String token);

    TokenEntity save(TokenEntity token);

    void delete(TokenEntity token);

    void deleteAll();
}
