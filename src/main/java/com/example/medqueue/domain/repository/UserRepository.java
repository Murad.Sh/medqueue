package com.example.medqueue.domain.repository;

import com.example.medqueue.domain.entity.UserEntity;
import jakarta.validation.constraints.NotBlank;
import java.util.Optional;

public interface UserRepository {
    Optional<UserEntity> findByEmail(@NotBlank String email);

    UserEntity save(UserEntity user);

    Boolean existsByEmail(@NotBlank String email);
}
